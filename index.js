// Imports the Google Cloud client library
const { PubSub } = require('@google-cloud/pubsub');
const { Firestore } = require('@google-cloud/firestore');
const axios = require('axios').default;

const pubsub = new PubSub();
const firestore = new Firestore();

const NBAsubscriptionName = 'projects/lunar-brace-213823/subscriptions/radar-nba-status-listener-sdp';
const subscriptionNBA = pubsub.subscription(NBAsubscriptionName);

const NFLsubscriptionName = 'projects/lunar-brace-213823/subscriptions/radar-nfl-status-listener-sdp';
const subscriptionNFL = pubsub.subscription(NFLsubscriptionName);

const NHLsubscriptionName = 'projects/lunar-brace-213823/subscriptions/radar-nhl-status-listener-sdp';
const subscriptionNHL = pubsub.subscription(NHLsubscriptionName);

const MLBsubscriptionName = 'projects/lunar-brace-213823/subscriptions/radar-mlb-status-listener-sdp';
const subscriptionMLB = pubsub.subscription(MLBsubscriptionName);

async function publishEventToPubsub(data, game_id, topic_name, type, league) {
    const customAttributes = {
        comp_id: game_id,
        type: type,
        league: league
    };

    const dataBuffer = Buffer.from(JSON.stringify(data));
    const messageId = await pubsub
        .topic(topic_name)
        .publish(dataBuffer, customAttributes)

    console.log(`Closed Message ${messageId} published to George.` + ' - ' + game_id + ' - ' + topic_name);
}

function postToPuncher(data, comp_id, topic, type, source, reload, league){
    var email = 'radarListener@sportsiq.ai';

    var url;
    url = 'https://us-central1-lunar-brace-213823.cloudfunctions.net/sportsiq_http_proxy2/publishToPubsub?comp_id=' + comp_id + '&topic=' + topic + '&type=' + type + '&source=' + source + '&reload=' + reload;
    url = url + '&league=' + league + '&email=' + email + '&data=' + JSON.stringify(data);

    axios.get(url)
        .then((res) => {
            if (res.status !== 200){
                data.error = res;
            }
        })
        .catch((error) => {
            data.error = error;
            console.log(error);
        })
}

async function saveToDataStore(game_id, status, period, period_end, collection) {
    // Obtain a document reference.
    console.log(collection, game_id, period, period_end, status);
    var reached_Q1;
    var Q1_ended;
    var reached_Q2;
    var Q2_ended;
    var reached_Q3;
    var Q3_ended;
    var reached_Q4;
    var Q4_ended;
    var inprogress_ed;
    var halftime_ed;
    var complete_ed;
    var closed_ed;
    var cancelled_ed;
    var old_period = 0;

    const document = firestore.collection(collection).doc(game_id);
    document.get().then(doc => {
        if (!doc.exists) {
            reached_Q1 = false;
            Q1_ended = false;
            reached_Q2 = false;
            Q2_ended = false;
            reached_Q3 = false;
            Q3_ended = false;
            reached_Q4 = false;
            Q4_ended = false;
            inprogress_ed = false;
            halftime_ed = false;
            complete_ed = false;
            closed_ed = false;
            cancelled_ed = false;
        } else {
            old_period = doc.data().latest_period;
            reached_Q1 = doc.data().reached_Q1;
            Q1_ended = doc.data().Q1_ended;
            reached_Q2 = doc.data().reached_Q2;
            Q2_ended = doc.data().Q2_ended;
            reached_Q3 = doc.data().reached_Q3;
            Q3_ended = doc.data().Q3_ended;
            reached_Q4 = doc.data().reached_Q4;
            Q4_ended = doc.data().Q4_ended;
            inprogress_ed = doc.data().inprogress_ed;
            halftime_ed = doc.data().halftime_ed;
            complete_ed = doc.data().complete_ed;
            closed_ed = doc.data().closed_ed;
            cancelled_ed = doc.data().cancelled_ed;
        }

        if (period === 1) {
            reached_Q1 = true;
            if (period_end === true) {
                Q1_ended = true;
            }
        }
        if (period === 2) {
            reached_Q2 = true;
            if (period_end === true) {
                Q2_ended = true;
            }
        }
        if (period === 3) {
            reached_Q3 = true;
            if (period_end === true) {
                Q3_ended = true;
            }
        }
        if (period === 4) {
            reached_Q4 = true;
            if (period_end === true) {
                Q4_ended = true;
            }
        }

        if (status === 'inprogress') {
            inprogress_ed = true;
        }
        if (status === 'halftime') {
            halftime_ed = true;
        }
        if (status === 'complete') {
            complete_ed = true;
        }
        if (status === 'closed') {
            closed_ed = true;
        }
        if (status === 'cancelled') {
            cancelled_ed = true;
        }

        document.set({
            comp_id: game_id,
            latest_status: status,
            latest_period: (period === 99)? old_period : period,
            reached_Q1: reached_Q1,
            Q1_ended: Q1_ended,
            reached_Q2: reached_Q2,
            Q2_ended: Q2_ended,
            reached_Q3: reached_Q3,
            Q3_ended: Q3_ended,
            reached_Q4: reached_Q4,
            Q4_ended: Q4_ended,
            inprogress_ed: inprogress_ed,
            halftime_ed: halftime_ed,
            complete_ed: complete_ed,
            closed_ed: closed_ed,
            cancelled_ed: cancelled_ed
        });
    })
}

async function saveToDataStoreMLB(game_id, status, period, double_header, collection) {
    // Obtain a document reference.
    console.log(collection, game_id, period, double_header, status);
    var reached_I1;
    var reached_I2;
    var reached_I3;
    var reached_I4;
    var reached_I5;
    var reached_I6;
    var reached_I7;
    var reached_I8;
    var reached_I9;
    var reached_I10;
    var double_header;
    var inprogress_ed;
    var complete_ed;
    var closed_ed;
    var cancelled_ed;

    var old_period = 0;

    const document = firestore.collection(collection).doc(game_id);
    document.get().then(doc => {
        if (!doc.exists) {
            reached_I1 = false;
            reached_I2 = false;
            reached_I3 = false;
            reached_I4 = false;
            reached_I5 = false;
            reached_I6 = false;
            reached_I7 = false;
            reached_I8 = false;
            reached_I9 = false;
            reached_I10 = false;
            double_header = double_header;
            inprogress_ed = false;
            complete_ed = false;
            closed_ed = false;
            cancelled_ed = false;
        } else {
            old_period = doc.data().latest_period;
            reached_I1 = doc.data().reached_I1;
            reached_I2 = doc.data().reached_I2;
            reached_I3 = doc.data().reached_I3;
            reached_I4 = doc.data().reached_I4;
            reached_I5 = doc.data().reached_I5;
            reached_I6 = doc.data().reached_I6;
            reached_I7 = doc.data().reached_I7;
            reached_I8 = doc.data().reached_I8;
            reached_I9 = doc.data().reached_I9;
            reached_I10 = doc.data().reached_I10;
            double_header = doc.data().double_header,
            inprogress_ed = doc.data().inprogress_ed;
            complete_ed = doc.data().complete_ed;
            closed_ed = doc.data().closed_ed;
            cancelled_ed = doc.data().cancelled_ed;
        }
        
        if (period === '1T' || period === '1B') {
            reached_I1 = true;
        }
        if (period === '2T' || period === '2B') {
            reached_I2 = true;
        }
        if (period === '3T' || period === '3B') {
            reached_I3 = true;
        }
        if (period === '4T' || period === '4B') {
            reached_I4 = true;
        }
        if (period === '5T' || period === '5B') {
            reached_I5 = true;
        }
        if (period === '6T' || period === '6B') {
            reached_I6 = true;
        }
        if (period === '7T' || period === '7B') {
            reached_I7 = true;
        }
        if (period === '8T' || period === '8B') {
            reached_I8 = true;
        }
        if (period === '9T' || period === '9B') {
            reached_I9 = true;
        }
        if (period === '10T' || period === '10B') {
            reached_I10 = true;
        }

        if (status === 'inprogress') {
            inprogress_ed = true;
        }
        if (status === 'complete') {
            complete_ed = true;
        }
        if (status === 'closed') {
            closed_ed = true;
        }
        if (status === 'cancelled') {
            cancelled_ed = true;
        }

        document.set({
            comp_id: game_id,
            latest_status: status,
            latest_period: (period === 99)? old_period : period,
            reached_I1: reached_I1,
            reached_I2: reached_I2,
            reached_I3: reached_I3,
            reached_I4: reached_I4,
            reached_I5: reached_I5,
            reached_I6: reached_I6,
            reached_I7: reached_I7,
            reached_I8: reached_I8,
            reached_I9: reached_I9,
            reached_I10: reached_I10,
            double_header: double_header,
            inprogress_ed: inprogress_ed,
            complete_ed: complete_ed,
            closed_ed: closed_ed,
            cancelled_ed: cancelled_ed
        });
    })
}

function generalMessageHandler(dataString, attributesObj, leagueName) {
    if (dataString){
        var dataObj = JSON.parse(dataString);
        var payload = dataObj.payload;

        if (payload) {
            var game = payload.game;
            var status = game? game.status : null;
            var game_id = game? game.id : null;

            var event =  payload.event;
            var event_type = event? event.event_type: null;
            var period = event? event.period : null;
            var period_sequence = period? period.sequence : null;
            var period_end = false;

            if (game && event && period_sequence) {
                if (status === 'inprogress' || status === 'halftime' || status === 'complete' || status === 'closed' || status === 'cancelled') {
                    if (event_type === 'endperiod') {
                        period_end = true;
                    }
                    // saveToDataStore(game_id, status, period_sequence, period_end, 'GAME_STATUS_'+leagueName);
                    if (status === 'closed') {
                        publishEventToPubsub(payload, game_id, 'radar.game_status', status, leagueName);
                    }
                }
            }
        }

        if (attributesObj && attributesObj.type === 'schedule') {
            var path
            if (leagueName === 'NBA') {
                var scheduled_games = dataObj.games;
                scheduled_games.forEach(game => {
                    var status = game.status;
                    var game_id = game.id;
                    if (status === 'complete' || status === 'closed' || status === 'cancelled') {
                        // saveToDataStore(game_id, status, 99, false, 'GAME_STATUS_'+leagueName);
                    }
                })
            } else if (leagueName === 'NFL') {
                path = 'https://us-central1-lunar-brace-213823.cloudfunctions.net/sportsiq_http_proxy2/eventNFL';
                axios.get(path)
                .then(function (response) {
                    var scheduleJson = response.data;
                    scheduleJson.games.forEach(game => {
                        var status = game.status;
                        var game_id = game.id;
                        if (status === 'complete' || status === 'closed' || status === 'cancelled') {
                            // saveToDataStore(game_id, status, 99, false, 'GAME_STATUS_'+leagueName);
                        }
                    })
                })
                .catch(function (error) {
                    console.log('error', error);
                });
            } else if (leagueName === 'NHL') {
                path = 'https://us-central1-lunar-brace-213823.cloudfunctions.net/sportsiq_http_proxy2/eventNHL';
                axios.get(path)
                .then(function (response) {
                    var scheduleJson = response.data;
                    scheduleJson.games.forEach(game => {
                        var status = game.status;
                        var game_id = game.id;
                        if (status === 'complete' || status === 'closed' || status === 'cancelled') {
                            // saveToDataStore(game_id, status, 99, false, 'GAME_STATUS_'+leagueName);
                        }
                    })
                })
                .catch(function (error) {
                    console.log('error', error);
                });
            }
        }

        try {
            if (attributesObj && attributesObj.type === 'time_change') {
                // postToPuncher({}, attributesObj.game_id, 'siq.control', 'player-props', 'betworks', true, attributesObj.league.toUpperCase());
            }
        } catch (error) {
            console.log(error);
        }
    }
}

function mlbMessageHandler(dataString, attributesObj, leagueName) {
    if (dataString){
        var dataObj = JSON.parse(dataString);
        var payload = dataObj.payload;

        if (payload) {
            var game = payload.game;
            var status = game? game.status : null;
            var game_id = game? game.id : null;
            var double_header = game? game.double_header : null;

            var event =  payload.event;
            var inning_number = event? event.inning : null;
            var inning_half = event? event.inning_half : null;
            var period = null;
            if (inning_number && inning_half) {
                period = inning_number + inning_half;
            }

            if (game && event && period) {
                if (status === 'inprogress' || status === 'halftime' || status === 'complete' || status === 'closed' || status === 'cancelled') {
                    // saveToDataStoreMLB(game_id, status, period, double_header, 'GAME_STATUS_'+leagueName);
                    if (status === 'closed') {
                        publishEventToPubsub(payload, game_id, 'radar_score.game_status', status, leagueName);
                    }
                }
            }
        }

        if (attributesObj && attributesObj.type === 'schedule') {
            var path = 'https://us-central1-lunar-brace-213823.cloudfunctions.net/sportsiq_http_proxy2/eventMLB';
            axios.get(path)
            .then(function (response) {
                var scheduleJson = response.data;
                scheduleJson.games.forEach(game => {
                    var status = game.status;
                    var game_id = game.id;
                    var double_header = game.double_header;
                    if (status === 'complete' || status === 'closed' || status === 'cancelled') {
                        // saveToDataStoreMLB(game_id, status, 99, double_header, 'GAME_STATUS_'+leagueName);
                    }
                })
            })
            .catch(function (error) {
                console.log('error', error);
            });
        }
    }
}

// Create an event handler to handle NBA messages
const messageHandlerNBA = message => {
    var dataString = `${message.data}`;
    var attributesObj = message.attributes;

    generalMessageHandler(dataString, attributesObj, 'NBA');

    message.ack();
};

// Create an event handler to handle NFL messages
const messageHandlerNFL = message => {
    var dataString = `${message.data}`;
    var attributesObj = message.attributes;

    generalMessageHandler(dataString, attributesObj, 'NFL');

    message.ack();
};

// Create an event handler to handle NHL messages
const messageHandlerNHL = message => {
    var dataString = `${message.data}`;
    var attributesObj = message.attributes;

    generalMessageHandler(dataString, attributesObj, 'NHL');

    message.ack();
};

// Create an event handler to handle MLB messages
const messageHandlerMLB = message => {
    var dataString = `${message.data}`;
    var attributesObj = message.attributes;

    mlbMessageHandler(dataString, attributesObj, 'MLB');

    message.ack();
};

// // Create an event handler to handle MLB messages
// const messageHandlerMLB = message => {
//     var dataString = `${message.data}`;

//     if (dataString){
//         var dataObj = JSON.parse(dataString);
//     }

//     message.ack();
// };

// Listen for NBA new messages until timeout is hit
subscriptionNBA.on(`message`, messageHandlerNBA)
subscriptionNBA.on('error', err => console.error(err));

// Listen for NFL new messages until timeout is hit
subscriptionNFL.on(`message`, messageHandlerNFL)
subscriptionNFL.on('error', err => console.error(err));

// Listen for NHL new messages until timeout is hit
subscriptionNHL.on(`message`, messageHandlerNHL)
subscriptionNHL.on('error', err => console.error(err));

// Listen for MLB new messages until timeout is hit
subscriptionMLB.on(`message`, messageHandlerMLB)
subscriptionMLB.on('error', err => console.error(err));

// // Listen for MLB new messages until timeout is hit
// subscriptionMLB.on(`message`, messageHandlerMLB)
// subscriptionMLB.on('error', err => console.error(err));